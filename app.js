"use strict";
//firebase
const admin = require('firebase-admin');
const functions = require('firebase-functions');
admin.initializeApp();
const auth = admin.auth();
const db = admin.firestore();

//debug
const debug = require('debug')('my-namespace')
const name = 'my-app'
debug('booting %s', name)

// Google Assistant deps
const actions_on_google_1 = require("actions-on-google");
const app = actions_on_google_1.dialogflow({ clientId: '727819076584-snre3cbtd39qu1o25qeu8eccgbdkbeto.apps.googleusercontent.com', debug: true });
const express = require('express')
const exapp = express()
//iscn
//const fs = require('fs');
/*let path = 'https://sis-scraper-rit-dup-2.herokuapp.com/get_sis_data/';
let usnip = '';
let dobip = '';*/
let convstatus = "init";

//const setup = require('./lib/setup');

app.intent('Start Signin', (conv, params, signin) => {


    conv.ask(new actions_on_google_1.SignIn('To use the service'),
        new actions_on_google_1.SimpleResponse({
            text: 'Signin initiated',
            speech: 'signin initiated',
        }),

    );

});


app.intent('Get Signin', (conv, params, signin) => {
    if (signin.status === 'OK') {
        const payload = conv.user.profile.payload;
        if (conv.user.storage.path == undefined || conv.user.storage.path == null) {
            return conv.ask(new actions_on_google_1.SimpleResponse({
                text: `Enter your USN?`,
                speech: `Enter your USN`,
            })
            );
            conv.followup("enterusn");
        }
        else {
            conv.ask(new actions_on_google_1.SimpleResponse({
                text: `What would you like to see?`,
                speech: `How can I help?`
            })
            );
            conv.ask(new actions_on_google_1.Suggestions(['CIE Report', 'Attendance']));
        }

    }
    else {

        return conv.ask(new actions_on_google_1.SimpleResponse({
            text: `I won't be able to save your data, but what do you want to do next?`,
            speech: `I won't be able to save your data, but what do you want to do next?`
        }
        ));
    }
});

app.intent('Default Welcome Intent', async (conv) => {
    conv.followup("startsignin");

}
);

app.catch((conv, error) => {
    console.error(error);
    conv.ask('Error : ' + error.message);
});
// Capture Intent

app.intent('USN Entry', async (conv, { usn }) => {
    conv.data.path = '';
    let path = 'https://sis-scraper-rit-dup-2.herokuapp.com/get_sis_data/';
    let usnip = '';
    if (usn == undefined || usn == null) {
        conv.ask(
            new actions_on_google_1.SimpleResponse({
                text: 'Input error, try again',
                speech: 'There was a problem processing your input',
            })
        )

    }
    else {
        conv.data.usn = usn;
        usnip = conv.data.usn.toString();

        conv.data.path = path + usnip;

        conv.ask(
            new actions_on_google_1.SimpleResponse({
                text: 'You entered ' + usn + ' , enter your DOB',
                speech: 'What is your Date of Birth?',
            })
        );




        //usnip = usnip.substring(0, usnip.length);

        console.log(usnip);
    }
});

app.intent('DOB entry', async (conv, { dob }) => {
    let dobip = '';
    conv.ask(new actions_on_google_1.SimpleResponse({
        text: 'You entered ' + dob + ' ',
        speech: 'Validating your credentials',
    }));
    conv.data.dob = '';
    conv.data.dob = dob.toString();
    dobip = conv.data.dob;

    dobip = dobip.substring(0, dobip.length - 15);//removes time from the dateobject
    conv.data.path = conv.data.path + "/" + dobip;
    const axios = require("axios");
    let res = await axios.get(conv.data.path);
    if (res == undefined || res == null || res.data.name == undefined) {
        return conv.ask(
            new actions_on_google_1.SimpleResponse({
                text: 'Incorrect USN/DOB. Please reenter by typing "My USN is <USN>"',
                speech: 'Invalid credentials, reenter by typing My USN is ',
            })


        )
        convstatus = "Incorrect Credentials";
        path = '';
    }
    else {
        conv.user.storage.path = conv.data.path;
        conv.ask(
            new actions_on_google_1.SimpleResponse({
                text: 'Validation succesfull',
                speech: 'Validation Successfull.How can I assist?',
            })


        )

    }
});
/* 
app.intent('Give me my Internals Report', async (conv) => {



    let a1 = "";
    //for testing only
    //path = 'https://sis-scraper-rit-dup-2.herokuapp.com/get_sis_data/1MS19CS129/2001-03-27';
    const axios = require("axios");
    let res = await axios.get(conv.user.storage.path);
    //console.log(res)

   
    for (var mark in res.data.marks) {
        if (res.data.marks[mark]['final cie'] == undefined) {
            a1 = a1.concat(res.data.marks[mark].name + " : N.A ");
        }
        else {
            a1 = a1.concat(res.data.marks[mark].name + " : " + res.data.marks[mark]['final cie'] + "");
        }
    }
    if (res.data.subject_list.length == 0 || res.data.marks.length != res.data.subject_list.length) {
        return conv.close(
            new actions_on_google_1.SimpleResponse({
                text: 'Details not updated in SIS, check again later',
                speech: 'Your subjects have not been fed into the database',
            })
        )
        path = '';
        convstatus = "details not updated";
    }
    else {
        convstatus = "SUCCESS";
        conv.ask(new actions_on_google_1.SimpleResponse({
            text: 'Here is the report',
            speech: 'here you go',
        }))
        conv.close(new actions_on_google_1.BasicCard({
            title: 'CIE Report|' + res.data.name + '|' + res.data.sem,
            text: a1,
            image: new actions_on_google_1.Image({
                url: 'https://formbuilder.ccavenue.com/media/media/Ramaiah_IOT_-_Logo.png',
                alt: 'MSRIT Logo',
            }),
            buttons: new actions_on_google_1.Button({
                title: 'Visit Website',
                url: 'http://parents.msrit.edu/index.php',
            }),
        })

        );

    }
});

 */
//Marks report list
app.intent('Give me my Internals Report', async (conv) => {




    
    const axios = require("axios");
    let res = await axios.get(conv.user.storage.path);


    let itemscar = {};


    res.data.marks.forEach((subj, index) => {
        console.log(`Building item from result: ${subj.name}`);
        itemscar[index] = {
            title: subj.name,
            //url: 'http://parents.msrit.edu/index.php',
            description: subj['final cie'],
        };



    });

    convstatus = "SUCCESS";
    conv.ask(new actions_on_google_1.SimpleResponse({
        text: 'CIE Report',
        speech: 'Here are your marks ',
    }))
    conv.close(new actions_on_google_1.List({
        title: 'CIE Marks | ' + res.data.name + ' | ' + res.data.sem,
        items: itemscar,
        /*items: [
          new actions_on_google_1.BrowseCarouselItem({
            title: 'Title of item 1',
            url: 'https://example.com',
            description: 'Description of item 1',
            
            footer: 'Item 1 footer',
          }),
          new actions_on_google_1.BrowseCarouselItem({
            title: 'Title of item 2',
            url: 'https://example.com',
            description: 'Description of item 2',
            
            footer: 'Item 2 footer',
          }),
        ],*/
    }));


});




//attendance report
/*
app.intent('Attendance', async (conv) => {



    let attendancereport = "";
    //for testing only
    //path = 'https://sis-scraper-rit-dup-2.herokuapp.com/get_sis_data/1MS19CS129/2001-03-27';
    const axios = require("axios");
    let res = await axios.get(conv.user.storage.path);
    //console.log(res)
    
    for (var subj in res.data.attendance) {
        
            attendancereport = attendancereport.concat(res.data.attendance[subj].name + " : " + res.data.attendance[subj].percentage + "  ");
        
    }
    if (res.data.subject_list.length == 0 || res.data.marks.length != res.data.subject_list.length) {
        return conv.close(
            new actions_on_google_1.SimpleResponse({
                text: 'Details not updated in SIS, check again later',
                speech: 'Your subjects have not been fed into the database',
            })
        )
        path = '';
        convstatus = "details not updated";
    }
    else {
        convstatus = "SUCCESS";
        conv.ask(new actions_on_google_1.SimpleResponse({
            text: 'Here you go',
            speech: 'here you go',
        }))
        conv.close(new actions_on_google_1.BasicCard({
            title: 'Attendance | ' + res.data.name + ' | ' + res.data.sem,
            text: attendancereport,
            image: new actions_on_google_1.Image({
                url: 'https://formbuilder.ccavenue.com/media/media/Ramaiah_IOT_-_Logo.png',
                alt: 'MSRIT Logo',
            }),
            buttons: new actions_on_google_1.Button({
                title: 'Visit Website',
                url: 'http://parents.msrit.edu/index.php',
            }),
        })

        );
        
    }
});
*/


//attendance report list

app.intent('Attendance', async (conv) => {




    //for testing only
    //path = 'https://sis-scraper-rit-dup-2.herokuapp.com/get_sis_data/1MS19CS129/2001-03-27';
    const axios = require("axios");
    let res = await axios.get(conv.user.storage.path);
    //console.log(res)

    let itemscar = {};


    res.data.attendance.forEach((subj, index) => {
        console.log(`Building item from result: ${subj.name}`);
        itemscar[index] = {
            title: subj.name,
            //url: 'http://parents.msrit.edu/index.php',
            description: subj.percentage,
        };



    });

    convstatus = "SUCCESS";
    conv.ask(new actions_on_google_1.SimpleResponse({
        text: 'Attendance Report',
        speech: 'Here is your attendance details',
    }))
    conv.ask(new actions_on_google_1.List({
        title: 'Attendance | ' + res.data.name + ' | ' + res.data.sem,
        items: itemscar,
        /*items: [
          new actions_on_google_1.BrowseCarouselItem({
            title: 'Title of item 1',
            url: 'https://example.com',
            description: 'Description of item 1',
            
            footer: 'Item 1 footer',
          }),
          new actions_on_google_1.BrowseCarouselItem({
            title: 'Title of item 2',
            url: 'https://example.com',
            description: 'Description of item 2',
            
            footer: 'Item 2 footer',
          }),
        ],*/
    }));
    conv.close(new actions_on_google_1.Suggestions('Exit'));


});



exapp.post('/webhook', express.json(), app)
exapp.get('/test',(req,res)=>{res.send("server online")})
/*exapp.get('/', (req, res) => {
    res.send("last checked usn was " + usnip + " and dob was" + dobip);
})*/
exapp.listen(process.env.PORT || 8000)
console.log("app.js is clean")
console.info("running")


