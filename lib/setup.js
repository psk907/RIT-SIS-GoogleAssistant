"use strict";
//firebase
const admin = require('firebase-admin');
const functions = require('firebase-functions');

const auth = admin.auth();
const db = admin.firestore();  

//Google Assistant deps
const actions_on_google_1 = require("actions-on-google");
const app = actions_on_google_1.dialogflow({  clientId: '727819076584-snre3cbtd39qu1o25qeu8eccgbdkbeto.apps.googleusercontent.com' ,debug: true });

//Express API
const express = require('express')
const exapp = express()

//Variable declaration
let path = 'https://sis-scraper-rit-dup-2.herokuapp.com/get_sis_data/';//link to webscraper
let usnip='';
let dobip='';
let convstatus="init";

function usnentry(conv){
    console.log("function call successfull");
    return conv.ask(new actions_on_google_1.SimpleResponse({
        text: 'Please enter your USN Number',
        speech: 'Heyy , What is your ID number ?',
    }));
}

console.log("abc");